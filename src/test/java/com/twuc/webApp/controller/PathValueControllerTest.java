package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class PathValueControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_pass_path_value() throws Exception {
        mockMvc.perform(get("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));
    }
    @Test
    void should_pass_path_value_with_int() throws Exception {
        mockMvc.perform(get("/api/users/s/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));
    }

    @Test
    void user_two_params() throws Exception {
        mockMvc.perform(get("/api/users/10/books/100"))
                .andExpect(status().isOk())
                .andExpect(content().string("userId=10,bookId=100"));
    }
}
