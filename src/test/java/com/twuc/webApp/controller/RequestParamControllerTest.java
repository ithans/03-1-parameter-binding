package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class RequestParamControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_use_request_params() throws Exception {
        mockMvc.perform(get("/api/student").param("studentId","10"))
                .andExpect(status().isOk())
                .andExpect(content().string("my studentId = 10"));

    }

    @Test
    void should_set_deafault_value() throws Exception {
        mockMvc.perform(get("/api/deafault"))
                .andExpect(status().isOk())
                .andExpect(content().string("dong.han"));
    }

    @Test
    void should_return() throws Exception {
        mockMvc.perform(get("/api/users/books")
                .param("book", "1", "2", "3")
        ).andExpect(status().isOk())
                .andExpect(content().string("3"));

    }

}
