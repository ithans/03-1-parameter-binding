package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@SpringBootTest
public class ParameterValidationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_check_validation() throws Exception {
        mockMvc.perform(get("/api/person")
                .content("{\"name\":\"dong.han\",\"age\":\"10\",\"personCard\":\"12345\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/person")
                .content("{\"age\":\"10\",\"personCard\":\"12345\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        mockMvc.perform(get("/api/person")
                .content("{\"name\":\"dong.han\",\"age\":\"101\",\"personCard\":\"12345\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        mockMvc.perform(get("/api/person")
                .content("{\"name\":\"dong.han\",\"age\":\"10\",\"personCard\":\"1234\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        mockMvc.perform(get("/api/person")
                .content("{\"name\":\"zhangsan\",\"email\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
}
