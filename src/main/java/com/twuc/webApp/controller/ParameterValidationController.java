package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.*;

@RestController
public class ParameterValidationController {

    @GetMapping("/api/person")
    public String getPerson(@RequestBody @Valid Person person){
        return person.getName();
    }


    static class Person{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getPersonCard() {
            return personCard;
        }

        public void setPersonCard(String personCard) {
            this.personCard = personCard;
        }

        @NotNull
        private String name;
        @Max(100)
        @Min(0)
        private Integer age;
        @Size(max = 10,min = 5)
        private String personCard;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Person(@NotNull String name, @Max(100) @Min(0) Integer age, @Size(max = 10, min = 5) String personCard, @Email String email) {
            this.name = name;
            this.age = age;
            this.personCard = personCard;
            this.email = email;
        }

        @Email
        private String email;


        public Person() {
        }

    }
}
