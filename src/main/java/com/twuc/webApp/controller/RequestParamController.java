package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.deser.impl.CreatorCandidate;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api")
@RestController
public class RequestParamController {
    @GetMapping("/student")
    public String getParam(@RequestParam() String studentId){
        return "my studentId = "+studentId;
    }
    @GetMapping("/deafault")
    public String getDeafaultValue(@RequestParam(value = "name",defaultValue = "dong.han")String name){
        return name;
    }
    @GetMapping("/api/users/books")
    public Integer getList(@RequestParam() List<String> book){
        return book.size();
    }
}
