package com.twuc.webApp.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.twuc.webApp.pojo.DateTime;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api")
public class RequestBodyController {
    @PostMapping("/datetimes")
    public void getDateTime(@RequestBody DateTime dateTime) {}
}
