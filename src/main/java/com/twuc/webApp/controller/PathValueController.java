package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class PathValueController {
    @GetMapping("/{userId}")
    public Integer PathValue(@PathVariable Integer userId) {
        return userId;
    }

    @GetMapping("/s/{userId}")
    public int pathValueWithInt(@PathVariable int userId) {
        return userId;
    }

    @GetMapping("/{userId}/books/{bookId}")
    public String getTwoParams(@PathVariable Integer userId, @PathVariable Integer bookId) {
        return "userId=" + userId + ",bookId=" + bookId;
    }
    @GetMapping("/books")
    public Integer getListMatch(@RequestParam List<String> book){
        return book.size();
    }
}
